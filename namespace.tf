# Isolate this project so we have the option to impose traffic boundary if we wish
resource "kubernetes_namespace" "btc" {
    metadata {
      name = "btc"
    }
}