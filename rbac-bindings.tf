resource "kubernetes_role_binding" "deployment" {
    metadata {
      name = "deployment"
      namespace = "btc"
    }
    
    role_ref {
      name = kubernetes_role.deployment.metadata[0].name
      kind = "Role"
      api_group = "rbac.authorization.k8s.io"
    }

    subject {
      kind = "Group"
      name = "gke-btc-deployment"
      api_group = "rbac.authorization.k8s.io"
    }

}

resource "kubernetes_role_binding" "application" {
    metadata {
      name = "application"
      namespace = "btc"
    }
    
    role_ref {
      name = kubernetes_cluster_role.application.metadata[0].name
      kind = "Role"
      api_group = "rbac.authorization.k8s.io"
    }

    subject {
      kind = "ServiceAccount"
      name = kubernetes_service_account.btc-service-account.metadata[0].name
      namespace = "btc"
    }

}