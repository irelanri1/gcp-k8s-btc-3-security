# gcp-k8s-btc-3-security

Security resources for a Kubernetes cluster hosted by GCP/GKE.

This project uses (only) the `Kubernetes` Terraform provider. It assumes you already have a K8S cluster (possibly managed by the Terraform `Google` provider as demonstrated by the previous project).

Features:

* creates RBAC roles to represent deployment and application (service account) purposes.
* binds the *deployment* role to a Google Group which I created through the Google Admin console (see https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control#groups-setup-gsuite).
* creates a K8S Service Account which it *binds* to a Google Service Account (which may or may not already exist). This means that the K8S Service Account will assume privileges from the Google Service Account in terms of using Google APIs.
* binds the ServiceAccount to the RBAC (Cluster) Role. This means that the ServiceAccount (which the workload will assume) has its K8S permissions managed by the Cluster Role, and wider Google permissions managed by the Google Service Account which will be attached to the `application` Google Group. That will be bound to an appropriate Google Role in the next project since we need to flip back using the `Google` Terraform provider to do so!






