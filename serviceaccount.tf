# The K8S service account is bound to a (possibly not yet existing) Google Service Account so that the former can
# assume the latter's credentials when using Google APIs

resource "kubernetes_service_account" "btc-service-account" {
  metadata {
    name = "btc-service-account"
    namespace = "btc"
    annotations = {
      "iam.gke.io/gcp-service-account" = "gcp-btc-service-account@${var.project_id}.iam.gserviceaccount.com"
    }
  }
  /*
  secret {
    name = "${kubernetes_secret.example.metadata.0.name}"
  }
  */
}

/*
resource "kubernetes_secret" "example" {
  metadata {
    name = "terraform-example"
  }
}
*/