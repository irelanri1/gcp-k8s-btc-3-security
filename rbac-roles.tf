
# Generic deployment role can do anything in a specified namespace
resource "kubernetes_role" "deployment" {
  metadata {
    name = "deployment"
    namespace = "btc"
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["*"]
  }
}

# Generic application role is read-only
resource "kubernetes_cluster_role" "application" {
  metadata {
    name = "application"
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["get", "list", "watch"]
  }
}